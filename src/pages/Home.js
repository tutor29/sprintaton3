import React from "react";
import NavBar from "../components/NavBar";
import SideBar from "../components/SideBar";

const Home = () => {
  return (
    <div className="wrapper">
      <NavBar />
      <SideBar />
    </div>
  );
};

export default Home;
