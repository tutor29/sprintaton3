import React, { Fragment } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Login from './components/Login';
import Home from './pages/Home';


function App() {
  return (
    <Fragment>
      <BrowserRouter>
        <Routes>
          <Route path="/" exact element={<Home/>}/>
          <Route path="/login" exact element={<Login/>}/>
        </Routes>
      </BrowserRouter>
    </Fragment>
  );
}

export default App;
